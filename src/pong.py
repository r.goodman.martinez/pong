import arcade
from random import randint
from config import *

class Pong(arcade.Window):
    def __init__(self):
        super().__init__(WIDTH, HEIGHT, TITLE)

        self.padels = arcade.SpriteList()
        self.player_score = 0
        self.computer_score = 0
        self.max_speed = 0
        self.speed = 0

    def setup(self):
        self.setup_player()
        self.setup_computer()
        self.setup_ball()
        self.ball_speed_multiplier = 1
        arcade.schedule(self.increase_ball_speed,5)

    def increase_ball_speed(self, delta_time:float):
        self.ball_speed_multiplier += 0.2

    def setup_player(self):
        self.player = arcade.Sprite("./assets/padel.png", SCALING)
        self.player.center_x = 40
        self.player.center_y = HEIGHT / 2
        self.padels.append(self.player)

    def setup_computer(self):
        self.computer = arcade.Sprite("./assets/padel.png", SCALING)
        self.computer.center_x = WIDTH - 40
        self.computer.center_y = HEIGHT / 2
        self.padels.append(self.computer)

    def draw_score(self):
        self.score_text = f"{self.player_score} : {self.computer_score}"
        self.speed_text = f"Speed: {self.speed}"
        self.max_speed_text = f"Max Speed: {self.max_speed}"
        arcade.draw_text(self.score_text, (WIDTH / 2)-10, HEIGHT - 50, arcade.color.WHITE)
        arcade.draw_text(self.speed_text, 60, HEIGHT - 50, arcade.color.WHITE)
        arcade.draw_text(self.max_speed_text, WIDTH - 160, HEIGHT - 50, arcade.color.WHITE)

    class BallSprite(arcade.Sprite):
        def update(self):
            super().update()
            if self.top >= HEIGHT:
                self.change_y = -1
            elif self.bottom <= 0:
                self.change_y = 1
            elif self.center_x >= WIDTH - 10:
                self.change_x = -1
            elif self.center_x <= 10:
                self.change_x = 1

    def setup_ball(self):
        self.ball = self.BallSprite("./assets/ball.png", SCALING * 1.2)
        self.ball.center_x = WIDTH / 2
        self.ball.center_y = randint(50, HEIGHT - 50)
        self.ball.change_x = 1
        self.ball.change_y = 1

    def on_key_press(self, key, modifiers):
        if key == arcade.key.UP:
            self.player.change_y = 3
        if key == arcade.key.DOWN:
            self.player.change_y = -3
          
    def on_key_release(self, key, modifiers):
        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player.change_y = 0

    def on_draw(self):
        arcade.start_render()
        self.padels.draw()
        self.ball.draw()
        self.draw_score()

    def check_collisions(self):
        if arcade.check_for_collision(self.ball, self.computer):
            self.ball.change_x = -1
            self.computer.change_y = 0
        if arcade.check_for_collision(self.ball, self.player):
            self.ball.change_x = 1
        if self.ball.left <= 20 or self.ball.right >= WIDTH - 20:
            if self.ball.left <= 20:
                self.computer_score += 1
            if self.ball.right >= WIDTH - 20:
                self.player_score += 1
            self.player.remove_from_sprite_lists()
            self.computer.remove_from_sprite_lists()
            self.ball.remove_from_sprite_lists()
            arcade.unschedule(self.increase_ball_speed)
            if self.speed > self.max_speed:
                self.max_speed = self.speed
            self.setup()

    def update_computer_padel(self):
        if self.ball.change_x > 0 and self.computer.center_y > self.ball.center_y:
            self.computer.change_y = -5
        elif self.ball.change_x > 0 and self.computer.center_y <= self.ball.center_y:
            self.computer.change_y = 5

    def on_update(self, delta_time: float):
        self.ball.update()
        self.update_computer_padel()
        self.ball.center_x += self.ball.change_x * self.ball_speed_multiplier
        self.ball.center_y += self.ball.change_y * self.ball_speed_multiplier
        self.speed = round(abs(self.ball.change_x * self.ball_speed_multiplier),2)
        if self.player.top + self.player.change_y <= HEIGHT and self.player.bottom + self.player.change_y >= 0:
            self.player.center_y += self.player.change_y
        if self.computer.top + self.computer.change_y <= HEIGHT and self.computer.bottom + self.computer.change_y >= 0:
            self.computer.center_y += self.computer.change_y
        self.check_collisions()

def main():
    game = Pong()
    game.setup()
    arcade.run()

if __name__ == "__main__":
    main()